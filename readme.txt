=== GRI - Fortune 500 ===
Contributors: Jesse Marple
Donate link:
Tags:
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

A custom plugin containing the Post Type and Advanced Custom Fields for the Fortune 500 database.

== Description ==

A custom plugin containing the Post Type and Advanced Custom Fields for the Fortune 500 database.

== Installation ==


== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision
