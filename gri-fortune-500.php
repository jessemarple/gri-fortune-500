<?php
/*
   Plugin Name: GRI - Fortune 500
   Plugin URI: http://wordpress.org/extend/plugins/gri-fortune-500/
   Version: 0.1
   Author: Jesse Marple
   Description: A custom plugin containing the Post Type and Advanced Custom Fields for the Fortune 500 database.
   Text Domain: gri-fortune-500
   License: GPLv3
  */

/*
    "WordPress Plugin Template" Copyright (C) 2019 Michael Simpson  (email : michael.d.simpson@gmail.com)

    This following part of this file is part of WordPress Plugin Template for WordPress.

    WordPress Plugin Template is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    WordPress Plugin Template is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Contact Form to Database Extension.
    If not, see http://www.gnu.org/licenses/gpl-3.0.html
*/

$GriFortune_minimalRequiredPhpVersion = '5.0';

/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function GriFortune_noticePhpVersionWrong() {
    global $GriFortune_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
      __('Error: plugin "GRI - Fortune 500" requires a newer version of PHP to be running.',  'gri-fortune-500').
            '<br/>' . __('Minimal version of PHP required: ', 'gri-fortune-500') . '<strong>' . $GriFortune_minimalRequiredPhpVersion . '</strong>' .
            '<br/>' . __('Your server\'s PHP version: ', 'gri-fortune-500') . '<strong>' . phpversion() . '</strong>' .
         '</div>';
}


function GriFortune_PhpVersionCheck() {
    global $GriFortune_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $GriFortune_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'GriFortune_noticePhpVersionWrong');
        return false;
    }
    return true;
}


/**
 * Initialize internationalization (i18n) for this plugin.
 * References:
 *      http://codex.wordpress.org/I18n_for_WordPress_Developers
 *      http://www.wdmac.com/how-to-create-a-po-language-translation#more-631
 * @return void
 */
function GriFortune_i18n_init() {
    $pluginDir = dirname(plugin_basename(__FILE__));
    load_plugin_textdomain('gri-fortune-500', false, $pluginDir . '/languages/');
}


//////////////////////////////////
// Run initialization
/////////////////////////////////

// Initialize i18n
add_action('plugins_loadedi','GriFortune_i18n_init');

// Run the version check.
// If it is successful, continue with initialization for this plugin
if (GriFortune_PhpVersionCheck()) {
    // Only load and run the init function if we know PHP version can parse it
    include_once('gri-fortune-500_init.php');
    include_once('includes/post_type.php');
    include_once('includes/categories.php');
    include_once('includes/company_category_meta.php');
    include_once('includes/incident_custom_fields.php');
    GriFortune_init(__FILE__);
}
