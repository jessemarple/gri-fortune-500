<?php
function cptui_register_my_taxes_incident_companies() {

	/**
	 * Taxonomy: Companies.
	 */

	$labels = array(
		"name" => __( "Companies"),
		"singular_name" => __( "Company"),
		"menu_name" => __( "Companies"),
		"all_items" => __( "All Companies"),
		"edit_item" => __( "Edit Company"),
		"view_item" => __( "View Company"),
		"update_item" => __( "Update Company Name"),
		"add_new_item" => __( "Add New Company"),
		"new_item_name" => __( "New Company Name"),
		"parent_item" => __( "Parent Company"),
		"parent_item_colon" => __( "Company:"),
		"search_items" => __( "Search Companies"),
		"popular_items" => __( "Popular Companies"),
		"separate_items_with_commas" => __( "Separate Companies with Commas"),
		"add_or_remove_items" => __( "Add or Remove Companies"),
		"choose_from_most_used" => __( "Choose From Most Used Companies"),
		"not_found" => __( "Company not found"),
		"no_terms" => __( "No Companies"),
		"items_list_navigation" => __( "Companies"),
		"items_list" => __( "Companies"),

	);

	$args = array(
		"label" => __( "Companies"),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'incident_companies', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "incident_companies",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "incident_companies", array( "fortune_incident" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_incident_companies' );


function cptui_register_my_taxes_incident_type() {

	/**
	 * Taxonomy: Types.
	 */

	$labels = array(
		"name" => __( "Types"),
		"singular_name" => __( "Type"),
		"menu_name" => __( "Types"),
		"all_items" => __( "All Types"),
		"edit_item" => __( "Edit Type"),
		"view_item" => __( "View Type"),
		"update_item" => __( "Update Type Name"),
		"add_new_item" => __( "Add New Type"),
		"new_item_name" => __( "New Type Name"),
		"parent_item" => __( "Parent Type"),
		"parent_item_colon" => __( "Type:"),
		"search_items" => __( "Search Types"),
		"popular_items" => __( "Popular Types"),
		"separate_items_with_commas" => __( "Separate Types with Commas"),
		"add_or_remove_items" => __( "Add or Remove Types"),
		"choose_from_most_used" => __( "Choose From Most Used Types"),
		"not_found" => __( "Type not found"),
		"no_terms" => __( "No types"),
		"items_list_navigation" => __( "Types"),
		"items_list" => __( "Types"),
	);

	$args = array(
		"label" => __( "Types"),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'incident_type', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "incident_type",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "incident_type", array( "fortune_incident" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_incident_type' );
