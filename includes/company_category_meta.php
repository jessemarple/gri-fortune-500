<?php

// A callback function to add a custom field
function fortune_taxonomy_custom_fields($tag) {
   // Check for existing taxonomy meta for the term you're editing
    $t_id = $tag->term_id; // Get the ID of the term you're editing
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
?>
<tr class="form-field">
    <th scope="row" valign="top">
        <label for="rank"><?php _e('Rank'); ?></label>
    </th>
    <td>
        <input type="number" name="term_meta[rank]" id="term_meta[rank]" size="25" style="width:60%;" value="<?php echo $term_meta['rank'] ? $term_meta['rank'] : ''; ?>"><br />
    </td>
</tr>

<?php
}


// A callback function to save our extra taxonomy field(s)
function fortune_save_tax_meta( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_term_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ){
            if ( isset( $_POST['term_meta'][$key] ) ){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        //save the option array
        update_option( "taxonomy_term_$t_id", $term_meta );
    }
}

add_action( 'incident_companies_edit_form_fields', 'fortune_taxonomy_custom_fields' );
add_action( 'edited_incident_companies', 'fortune_save_tax_meta', 10, 2 );
