<?php
function cptui_register_my_cpts_fortune_incident() {

	/**
	 * Post Type: Fortune Incidents.
	 */

	$labels = array(
		"name" => __( "Fortune Incidents", "twentynineteen" ),
		"singular_name" => __( "Fortune Incident", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Fortune Incidents", "twentynineteen" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "fortune_incident", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-flag",
		"supports" => array( "title", "editor", "custom-fields" ),
	);

	register_post_type( "fortune_incident", $args );
}

add_action( 'init', 'cptui_register_my_cpts_fortune_incident' );
